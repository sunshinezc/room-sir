# room-sir

#### 介绍
room-sir系统是基于springboot，springcloud stream + kafka开发的一套分布式房间消息分发系统。系统支持分布式，多实例部署，能实现用户无感知的故障转移和容灾机制，保证了高可用和高性能的消息分析机制，适用于聊天室，互动游戏后台（例如棋牌游戏）等等。

#### 软件架构
软件架构说明
1. access-system是用户连接和session管理模块，本系统使用websocket实现客户端浏览器和服务的的通信。
2. room-system是房间消息分发主系统，服务维护房间用户相互关系和address关系。
3. user-system是用户业务模块，包括登录系统，分发token令牌等业务。
4. 本系统内嵌了简单的web页面以实现用户的方便调试，和直观查看运行效果。

#### 安装教程

1. 推荐使用linux cenos版本进行安装部署，编辑器推荐jidea进行开发调试。
2. 系统要预装jdk1.8以上，zookeeper，kafka，redis服务。
3. 启动access-system和room-system模块，其中各个模块可以启动多个实例。
4. accees-system的配置文件application.yml中，属性 input:destination: topic-access-service 要保持唯一，特殊是启动多实例是要注意修改该配置和端口号。
5. room-system启动多实例时不需要修改任何配置。

#### 使用说明

1. 启动zookeeper，kafka，redis服务，修改系统hosts文件指定zk和kafka的映射地址。例如：
    118.xxx.93.xx kafka zk db rd
2. 启动access-system和room-system，启动不分先后顺序。
3. 使用本地浏览器访问地址：http://localhost:8080/room/demo。
4. 点击页面链接开启websocket，点击登录房间，然后点发送消息测试。

#### 效果图
1、<img src="https://gitee.com/lzht_pbjh/room-sir/raw/master/img/1.jpg">
2、<img src="https://gitee.com/lzht_pbjh/room-sir/raw/master/img/2.jpg">
 

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 特别关注
   如果后续该项目关注度客观，将逐步开源一套棋牌游戏项目，该项目属于本人业余时间完成，有完全自主产权，试运营过一段时间，前端是layabox游戏引擎开发，后端java分布式消息分发系统。后续将以该room-sir系统重构后台，欢迎关注。
   先上效果图：
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/1220/112513_0fff3bf8_1427595.jpeg "a.jpg")
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/1220/112530_6c7779b5_1427595.jpeg "b.jpg")
    ![输入图片说明](https://images.gitee.com/uploads/images/2018/1220/112542_2b628e38_1427595.jpeg "c.jpg")


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)