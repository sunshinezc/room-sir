package com.lzht.games.access;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Create by weishuaiding on 2018/11/29 17:22
 */
@SpringBootApplication
public class AccessServer {
    private static Logger logger = LoggerFactory.getLogger(AccessServer.class);

    public static void main(String[] args) {
        SpringApplication.run(AccessServer.class, args);
        logger.info("AccessServer start...");
    }
}
