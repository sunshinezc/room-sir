package com.lzht.games.access.cache;

import org.springframework.web.socket.WebSocketSession;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Create by weishuaiding on 2018/9/3 14:52
 */
public class SessionCache {
    private static Map<String, WebSocketSession> sessionMap = new ConcurrentHashMap<>();
    private static Map<String, String> sidMap = new ConcurrentHashMap<>();

    public static void addSession(String sessionId, WebSocketSession session) {
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        sidMap.put(sessionId, uuid);
        sessionMap.put(uuid, session);
    }

    public static WebSocketSession getSession(String sessionId) {
//        String uuid = sidMap.get(sessionId);
        return sessionMap.get(sessionId);
    }

    public static void removeSession(String sessionId) {
        String uuid = sidMap.remove(sessionId);
        sessionMap.remove(uuid);
    }

    public static String getSessionUUid(String sessionId) {
        return sidMap.get(sessionId);
    }
}
