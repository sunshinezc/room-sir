package com.lzht.games.access.handler;

import com.lzht.games.access.stream.SendService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Create by weishuaiding on 2018/11/29 17:34
 */
@Controller
@RequestMapping("/room")
public class RoomController {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private SendService sendService;

    @Value("${spring.cloud.stream.bindings.input.destination}")
    private String accessId;

    @RequestMapping("/demo")
    public String test() {
        return "room";
    }

//    @GetMapping("/login")
//    public String login() {
//        RequestMsg req = new RequestMsg();
//        req.setMoudleId(2);
//        req.setMsgCode(1);
//        req.setMessage("登录房间请求");
//        req.setAccessId(accessId);
//        sendService.notifyRoom(JSONObject.toJSONString(req));
//
//        logger.info("登录房间请求...");
//        return "login ok";
//    }
//
//    @GetMapping("/logout")
//    public String logout() {
//        RequestMsg req = new RequestMsg();
//        req.setMoudleId(2);
//        req.setMsgCode(2);
//        req.setMessage("退出房间请求");
//        req.setAccessId(accessId);
//        sendService.notifyRoom(JSONObject.toJSONString(req));
//
//        logger.info("退出房间请求");
//        return "logout ok";
//    }
//
//    @GetMapping("/sendMsg")
//    public String sendMsg() {
//        RequestMsg req = new RequestMsg();
//        req.setMoudleId(2);
//        req.setMsgCode(3);
//        req.setMessage("发送房间消息请求");
//        req.setAccessId(accessId);
//        sendService.notifyRoom(JSONObject.toJSONString(req));
//
//        logger.info("发送房间消息请求");
//        return "sendMsg ok";
//    }


}
