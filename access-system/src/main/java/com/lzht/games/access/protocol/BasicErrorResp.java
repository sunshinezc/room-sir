package com.lzht.games.access.protocol;

import com.lzht.games.common.annotation.Message;
import com.lzht.games.common.common.BasicMsg;
import com.lzht.games.common.constant.ModuleConstant;
import org.springframework.stereotype.Component;

/**
 * Create by weishuaiding on 2018/12/10 15:14
 */
@Component
@Message(moduleId = ModuleConstant.MODULE_ACCESS, msgCode = 10001)
public class BasicErrorResp extends BasicMsg {

}
