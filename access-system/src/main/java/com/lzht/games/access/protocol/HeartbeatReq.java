package com.lzht.games.access.protocol;

import com.lzht.games.common.annotation.Message;
import com.lzht.games.common.common.BasicMsg;
import com.lzht.games.common.constant.ModuleConstant;
import org.springframework.stereotype.Component;

/**
 * Create by weishuaiding on 2018/12/18 14:59
 */
@Component
@Message(moduleId = ModuleConstant.MODULE_ACCESS, msgCode = 10003)
public class HeartbeatReq extends BasicMsg {

}
