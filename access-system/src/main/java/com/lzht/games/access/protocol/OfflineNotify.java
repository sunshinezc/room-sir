package com.lzht.games.access.protocol;

import com.lzht.games.common.annotation.Message;
import com.lzht.games.common.common.BasicMsg;
import com.lzht.games.common.constant.ModuleConstant;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.stereotype.Component;

/**
 * Create by weishuaiding on 2018/12/17 17:19
 * 离线通知
 */
@Component
@Message(moduleId = ModuleConstant.MODULE_ACCESS, msgCode = 10002)
public class OfflineNotify extends BasicMsg {
    private String sessionId;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("sessionId", sessionId)
                .toString();
    }
}
