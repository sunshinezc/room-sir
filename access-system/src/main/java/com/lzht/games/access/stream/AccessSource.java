package com.lzht.games.access.stream;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;

/**
 * Create by weishuaiding on 2018/11/30 14:03
 */
@Service
public interface AccessSource {

    @Output("userOutput")
    MessageChannel userChannel();

    @Output("roomOutput")
    MessageChannel roomChannel();

    @Output("offLineOutput")
    MessageChannel offLineChannel();
}
