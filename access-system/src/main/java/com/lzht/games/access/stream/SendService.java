package com.lzht.games.access.stream;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;

/**
 * Create by weishuaiding on 2018/11/29 18:16
 */
@EnableBinding(AccessSource.class)
public class SendService {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private AccessSource accessSource;

    public <T> void notifyUser(T msg) {
        logger.info("通知用户系统：{}", msg);
        accessSource.userChannel().send(MessageBuilder.withPayload(msg).build());
    }

    public <T> void notifyRoom(T msg) {
        logger.info("通知房间系统：{}", msg);
        accessSource.roomChannel().send(MessageBuilder.withPayload(msg).build());
    }

    public <T> void notifyOffLine(T msg) {
        logger.info("离线通知系统：{}", msg);
        accessSource.offLineChannel().send(MessageBuilder.withPayload(msg).build());
    }
}
