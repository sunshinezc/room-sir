package com.lzht.games.access.util;

import com.lzht.games.access.cache.SessionCache;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;

import java.io.IOException;

/**
 * Create by weishuaiding on 2018/12/10 16:47
 */
public class SessionUtil {
    private static Logger logger = LoggerFactory.getLogger(SessionUtil.class);

    public static <T> void sendMessage(String sessionId, T message) {
        WebSocketSession session = SessionCache.getSession(sessionId);
        if (session != null) {
            try {
                session.sendMessage(new TextMessage(message.toString()));
            } catch (IOException e) {
                logger.error("sendMessage fail:{}", ExceptionUtils.getStackTrace(e));
            }
        }
    }
}
