package com.lzht.games.common.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Create by weishuaiding on 2018/12/5 19:04
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.TYPE})
public @interface Message {
    int moduleId(); //模块号

    int msgCode(); //消息号
}
