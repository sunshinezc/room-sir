package com.lzht.games.common.common;

import com.lzht.games.common.header.NotifyHeader;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Create by weishuaiding on 2018/12/5 19:26
 */
public class NotifyMsg<T> implements Cloneable {
    private NotifyHeader header;
    private T notifyData;

    public NotifyHeader getHeader() {
        return header;
    }

    public void setHeader(NotifyHeader header) {
        this.header = header;
    }

    public T getNotifyData() {
        return notifyData;
    }

    public void setNotifyData(T notifyData) {
        this.notifyData = notifyData;
    }

    public void getReqValue() {

    }

    @Override
    public NotifyMsg<T> clone() {
        try {
            return (NotifyMsg<T>) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("header", header)
                .append("notifyData", notifyData)
                .toString();
    }
}
