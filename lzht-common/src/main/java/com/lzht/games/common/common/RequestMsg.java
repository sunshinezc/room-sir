package com.lzht.games.common.common;

import com.lzht.games.common.header.ReqHeader;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Create by weishuaiding on 2018/12/5 19:09
 */
public class RequestMsg extends BasicMsg {
    private ReqHeader header;
    private BasicMsg reqData;

    public BasicMsg getReqData() {
        return reqData;
    }

    public void setReqData(BasicMsg reqData) {
        this.reqData = reqData;
    }

    public ReqHeader getHeader() {
        return header;
    }

    public void setHeader(ReqHeader header) {
        this.header = header;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("header", header)
                .append("reqData", reqData)
                .toString();
    }
}
