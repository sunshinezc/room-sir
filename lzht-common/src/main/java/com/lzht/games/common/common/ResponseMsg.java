package com.lzht.games.common.common;

import com.lzht.games.common.annotation.Message;
import com.lzht.games.common.constant.Results;
import com.lzht.games.common.header.ReqHeader;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Create by weishuaiding on 2018/12/5 19:30
 */
public class ResponseMsg extends BasicMsg {
    private ReqHeader header;
    private Integer resCode;
    private String resMsg;
    private BasicMsg resData;

    public ResponseMsg() {
    }

    public ResponseMsg(BasicMsg resData, String requestId) {
        this.header = header;
        this.resData = resData;
    }

    public Integer getResCode() {
        return resCode;
    }

    public void setResCode(Integer resCode) {
        this.resCode = resCode;
    }

    public String getResMsg() {
        return resMsg;
    }

    public void setResMsg(String resMsg) {
        this.resMsg = resMsg;
    }

    public BasicMsg getResData() {
        return resData;
    }

    public void setResData(BasicMsg resData) {
        this.resData = resData;
    }

    public ReqHeader getHeader() {
        return header;
    }

    public void setHeader(ReqHeader header) {
        this.header = header;
    }

    public ResponseMsg ok(BasicMsg respData, String requestId) {
        Message message = respData.getClass().getAnnotation(Message.class);
        header = new ReqHeader(message.moduleId(), message.msgCode());
        this.header.setRequestId(requestId);
        this.resCode = Results.OK.getErrorCode();
        this.resMsg = Results.OK.getErrorMsg();
        this.resData = respData;
        return this;
    }

    public ResponseMsg systemError(BasicMsg respData, String requestId) {
        Message message = respData.getClass().getAnnotation(Message.class);
        header = new ReqHeader(message.moduleId(), message.msgCode());
        this.header.setRequestId(requestId);
        this.resCode = Results.ERROR_SYSTEM.getErrorCode();
        this.resMsg = Results.ERROR_SYSTEM.getErrorMsg();
        this.resData = respData;
        return this;
    }

    public ResponseMsg paramError(BasicMsg respData, String requestId) {
        Message message = respData.getClass().getAnnotation(Message.class);
        header = new ReqHeader(message.moduleId(), message.msgCode());
        this.header.setRequestId(requestId);
        this.resCode = Results.ERROR_PARAM.getErrorCode();
        this.resMsg = Results.ERROR_PARAM.getErrorMsg();
        this.resData = respData;
        return this;
    }

    public ResponseMsg error(Results result, BasicMsg respData, String requestId) {
        Message message = respData.getClass().getAnnotation(Message.class);
        header = new ReqHeader(message.moduleId(), message.msgCode());
        this.header.setRequestId(requestId);
        this.resCode = result.getErrorCode();
        this.resMsg = result.getErrorMsg();
        this.resData = respData;
        return this;
    }


    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("header", header)
                .append("resCode", resCode)
                .append("resMsg", resMsg)
                .append("resData", resData)
                .toString();
    }
}
