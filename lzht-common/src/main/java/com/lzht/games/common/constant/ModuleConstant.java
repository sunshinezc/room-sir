package com.lzht.games.common.constant;

/**
 * Create by weishuaiding on 2018/12/5 20:11
 */
public class ModuleConstant {
    public static final int MODULE_ACCESS = 10000;
    public static final int MODULE_USER = 20000;
    public static final int MODULE_ROOM = 30000;
}
