package com.lzht.games.common.constant;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Create by weishuaiding on 2018/11/19 15:40
 */
public enum Results {
    OK(0, "succeed."),
    ERROR_SYSTEM(1, "亲,系统出了些小问题哦，请重新操作."),
    ERROR_PARAM(2, "亲,您提交的数据缺少必要参数，请重新操作."),
    ERROR_TIMEOUT(3, "亲，服务器连接超时，请您重新操作."),
    ERROR_001(1001, "发消息错误：你未登陆该房间！");

    Results(Integer errorCode, String errorMsg) {
        this.errorCode = errorCode;
        this.errorMsg = errorMsg;
    }

    private Integer errorCode;
    private String errorMsg;

    public Integer getErrorCode() {
        return errorCode;
    }

    public String getErrorMsg() {
        return errorMsg;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("errorCode", errorCode)
                .append("errorMsg", errorMsg)
                .toString();
    }
}
