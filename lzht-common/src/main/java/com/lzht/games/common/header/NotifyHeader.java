package com.lzht.games.common.header;

import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Create by weishuaiding on 2018/12/5 19:41
 */
public class NotifyHeader {
    private String accessId;
    private String sessionId;
    private Long instanceId; //实例id

    public String getAccessId() {
        return accessId;
    }

    public void setAccessId(String accessId) {
        this.accessId = accessId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public Long getInstanceId() {
        return instanceId;
    }

    public void setInstanceId(Long instanceId) {
        this.instanceId = instanceId;
    }

    public NotifyHeader(String accessId, String sessionId) {
        this.accessId = accessId;
        this.sessionId = sessionId;
    }

    public NotifyHeader() {
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("accessId", accessId)
                .append("sessionId", sessionId)
                .append("instanceId", instanceId)
                .toString();
    }
}
