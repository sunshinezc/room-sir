package com.lzht.games.common.header;

import com.lzht.games.common.annotation.Message;
import com.lzht.games.common.common.BasicMsg;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Create by weishuaiding on 2018/12/5 19:32
 */
public class ReqHeader {
    private Integer moduleId; //模块号
    private Integer msgCode; //消息号
    private String requestId; //请求序列号，唯一值

    public Integer getModuleId() {
        return moduleId;
    }

    public void setModuleId(Integer moduleId) {
        this.moduleId = moduleId;
    }

    public Integer getMsgCode() {
        return msgCode;
    }

    public void setMsgCode(Integer msgCode) {
        this.msgCode = msgCode;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public ReqHeader() {
    }

    public ReqHeader(Integer moduleId, Integer msgCode) {
        this.moduleId = moduleId;
        this.msgCode = msgCode;
    }

    public ReqHeader(BasicMsg msg) {
        Message message = msg.getClass().getAnnotation(Message.class);
        this.moduleId = message.moduleId();
        this.msgCode = message.msgCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("moduleId", moduleId)
                .append("msgCode", msgCode)
                .append("requestId", requestId)
                .toString();
    }
}
