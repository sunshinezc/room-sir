package com.lzht.games.common.util;

import com.alibaba.fastjson.JSONObject;

/**
 * Create by weishuaiding on 2018/12/10 15:25
 */
public class JsonUtil {

    public static <T> String objectToJson(T value) {
        return JSONObject.toJSONString(value);
    }

    public static <T> T jsonToObject(String json, Class<T> clazz) {
        return JSONObject.parseObject(json, clazz);
    }
}
