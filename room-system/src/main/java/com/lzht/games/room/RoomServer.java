package com.lzht.games.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Create by weishuaiding on 2018/11/29 17:22
 */
@SpringBootApplication
public class RoomServer {
    private static Logger logger = LoggerFactory.getLogger(RoomServer.class);

    public static void main(String[] args) {
        SpringApplication.run(RoomServer.class, args);
        logger.info("RoomServer start...");
    }
}
