package com.lzht.games.room.cache;

import com.lzht.games.room.model.Address;

import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Create by weishuaiding on 2018/12/10 17:39
 */
public class RoomCache {
    //Map<roomId, List<Address>>
    private static Map<Integer, Set<Address>> addressMap = new ConcurrentHashMap<>();
    private static Map<String, Address> sessionAddressMap = new ConcurrentHashMap<>();
    private static ReentrantLock lock = new ReentrantLock();

    public static void addRoomUser(Integer roomId, Address address) {
        boolean isLock = lock.tryLock();
        if (isLock) {
            try {
                Set<Address> addressSet = getRoomUsers(roomId);
                if (addressSet == null) {
                    addressSet = Collections.synchronizedSet(new HashSet<>());
                    addressMap.put(roomId, addressSet);
                }
                addressSet.add(address);
                sessionAddressMap.put(address.getSessionId(), address);
            } finally {
                lock.unlock();
            }
        } else {
            addRoomUser(roomId, address);
        }
    }

    public static Set<Address> getRoomUsers(Integer roomId) {
        return addressMap.get(roomId);
    }

    public static Address removeRoomUser(Integer roomId, Integer userId) {
        Set<Address> addresses = addressMap.get(roomId);
        Address addr = null;
        if (addresses != null) {
            for (Address address : addresses) {
                if (address.getUserId() == userId.intValue()) {
                    addresses.remove(address);
                    addr = address;
                }
            }
            if (addresses.size() == 0) {
                addressMap.remove(roomId);
            }
            if (addr != null) {
                sessionAddressMap.remove(addr.getSessionId());
            }
        }
        return addr;
    }

    public static void initRoomUsers(Integer roomId, Set<Address> addressSet) {
        boolean isLock = lock.tryLock();
        if (isLock) {
            try {
                Set<Address> synSet = Collections.synchronizedSet(new HashSet<>());
                synSet.addAll(addressSet);
                addressMap.put(roomId, synSet);
                for (Address address : synSet) {
                    sessionAddressMap.put(address.getSessionId(), address);
                }
            } finally {
                lock.unlock();
            }
        } else {
            initRoomUsers(roomId, addressSet);
        }
    }

    public static Address getAddressBySessionId(String sessionId) {
        return sessionAddressMap.get(sessionId);
    }
}
