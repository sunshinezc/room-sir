package com.lzht.games.room.common;

/**
 * Create by weishuaiding on 2018/12/5 14:32
 */
public class Topic {
    public static final String INPUT_LOGIN = "loginInput";
    public static final String INPUT_LOGOUT = "logoutInput";
    public static final String INPUT_MESSAGE = "messageInput";
    public static final String INPUT_OFFLINE = "offlineInput";

    public static final String OUTPUT_LOGIN = "loginOutput";
    public static final String OUTPUT_LOGOUT = "logoutOutput";
    public static final String OUTPUT_MESSAGE = "messageOutput";

}
