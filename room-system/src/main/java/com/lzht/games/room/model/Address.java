package com.lzht.games.room.model;

import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.Objects;

/**
 * Create by weishuaiding on 2018/12/10 17:40
 */
public class Address {
    private Integer userId;
    private Integer roomId;
    private String accessId;
    private String sessionId;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getAccessId() {
        return accessId;
    }

    public void setAccessId(String accessId) {
        this.accessId = accessId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(userId, address.userId) &&
                Objects.equals(roomId, address.roomId) &&
                Objects.equals(accessId, address.accessId) &&
                Objects.equals(sessionId, address.sessionId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, roomId, accessId, sessionId);
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("userId", userId)
                .append("roomId", roomId)
                .append("accessId", accessId)
                .append("sessionId", sessionId)
                .toString();
    }
}
