package com.lzht.games.room.protocol;

import com.lzht.games.common.annotation.Message;
import com.lzht.games.common.common.BasicMsg;
import com.lzht.games.common.constant.ModuleConstant;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.stereotype.Component;

/**
 * Create by weishuaiding on 2018/12/5 20:08
 */
@Component
@Message(moduleId = ModuleConstant.MODULE_ROOM, msgCode = 30001)
public class LoginReq extends BasicMsg {
    private Integer roomId;
    private Integer userId;
    private String nickname;
    private String icon;

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("roomId", roomId)
                .append("userId", userId)
                .append("nickname", nickname)
                .append("icon", icon)
                .toString();
    }
}
