package com.lzht.games.room.protocol;

import com.lzht.games.common.annotation.Message;
import com.lzht.games.common.common.BasicMsg;
import com.lzht.games.common.constant.ModuleConstant;
import org.springframework.stereotype.Component;

/**
 * Create by weishuaiding on 2018/12/5 20:08
 */
@Component
@Message(moduleId = ModuleConstant.MODULE_ROOM, msgCode = 30002)
public class LoginResp extends BasicMsg {

}
