package com.lzht.games.room.protocol;

import com.lzht.games.common.annotation.Message;
import com.lzht.games.common.common.BasicMsg;
import com.lzht.games.common.constant.ModuleConstant;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.springframework.stereotype.Component;

/**
 * Create by weishuaiding on 2018/12/5 20:08
 */
@Component
@Message(moduleId = ModuleConstant.MODULE_ROOM, msgCode = 30005)
public class SendMsgReq extends BasicMsg {
    private Integer roomId;
    private Integer userId;
    private String message; //消息内容

    public Integer getRoomId() {
        return roomId;
    }

    public void setRoomId(Integer roomId) {
        this.roomId = roomId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("roomId", roomId)
                .append("userId", userId)
                .append("message", message)
                .toString();
    }
}
