package com.lzht.games.room.service;

import com.lzht.games.room.model.Address;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Create by weishuaiding on 2018/12/11 17:42
 */
@Service
public interface RoomService {

    void addRoomUser(Integer roomId, Address address);

    void removeRoomUser(Integer roomId, Integer userId);

    Set<Address> getRoomUsers(Integer roomId);
}
