package com.lzht.games.room.stream;

import com.lzht.games.room.common.Topic;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;

/**
 * Create by weishuaiding on 2018/12/5 14:29
 */
@Service
public interface RoomSource {
    @Output(Topic.OUTPUT_LOGIN)
    MessageChannel loginChannel();

    @Output(Topic.OUTPUT_LOGOUT)
    MessageChannel logoutChannel();

    @Output(Topic.OUTPUT_MESSAGE)
    MessageChannel messageChannel();

    @Input(Topic.INPUT_LOGIN)
    MessageChannel login();

    @Input(Topic.INPUT_LOGOUT)
    MessageChannel logout();

    @Input(Topic.INPUT_MESSAGE)
    MessageChannel message();
    
    @Input(Sink.INPUT)
    MessageChannel room();

    @Input(Topic.INPUT_OFFLINE)
    MessageChannel offline();
}
