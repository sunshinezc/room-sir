package com.lzht.games.room.stream;


import com.lzht.games.common.common.NotifyMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;

/**
 * Create by weishuaiding on 2018/12/5 14:43
 */
@EnableBinding(RoomSource.class)
public class SendService {
    private Logger logger = LoggerFactory.getLogger(getClass());
    @Autowired
    private RoomSource roomSource;

    public void loginNotify(NotifyMsg message) {
        roomSource.loginChannel().send(MessageBuilder.withPayload(message).build());
        logger.info("发送登录通知：{}", message);
    }

    public void logoutNotify(NotifyMsg message) {
        roomSource.logoutChannel().send(MessageBuilder.withPayload(message).build());
        logger.info("发送退出通知：{}", message);
    }

    public void messageNotify(NotifyMsg message) {
        roomSource.messageChannel().send(MessageBuilder.withPayload(message).build());
        logger.info("发送消息通知：{}", message);
    }
}
