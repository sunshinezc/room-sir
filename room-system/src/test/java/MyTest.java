import com.alibaba.fastjson.JSONObject;
import com.lzht.games.common.annotation.Message;
import com.lzht.games.common.common.RequestMsg;
import com.lzht.games.common.header.ReqHeader;
import com.lzht.games.room.model.Address;
import com.lzht.games.room.protocol.LogoutReq;
import com.lzht.games.room.protocol.SendMsgReq;
import org.apache.commons.lang3.RandomUtils;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

/**
 * Create by weishuaiding on 2018/12/10 18:33
 */
public class MyTest {

    @Test
    public void test() {
        LogoutReq req = new LogoutReq();
        req.setUserId(10000);
        req.setRoomId(10000);

        RequestMsg msg = new RequestMsg();
        msg.setReqData(req);

        Message message = req.getClass().getAnnotation(Message.class);
        ReqHeader header = new ReqHeader(message.moduleId(), message.msgCode());
        header.setRequestId("1234567890");
        msg.setHeader(header);

        System.out.println(JSONObject.toJSONString(msg));
    }

    @Test
    public void test2() {
        SendMsgReq req = new SendMsgReq();
        req.setUserId(10000);
        req.setRoomId(10000);
        req.setMessage("来来来，来打牛啦！");

        RequestMsg msg = new RequestMsg();
        msg.setReqData(req);

        Message message = req.getClass().getAnnotation(Message.class);
        ReqHeader header = new ReqHeader(message.moduleId(), message.msgCode());
        header.setRequestId("1234567890");
        msg.setHeader(header);

        System.out.println(JSONObject.toJSONString(msg));
    }

    @Test
    public void test3() {
        for (int i = 0; i < 10; i++)
            System.out.println(RandomUtils.nextLong());
    }

    @Test
    public void test4() {
        Set<Address> addressSet = new HashSet<>();

        for (int i = 0; i < 5; i++) {
            Address address = new Address();
            address.setUserId(10000);
            address.setRoomId(10000);
            address.setSessionId("ggggggggg");
            address.setAccessId("op-1pop");

            addressSet.add(address);
        }

        System.out.println(addressSet.size());
    }
}
