package com.lzht.games.room;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Create by weishuaiding on 2018/11/29 17:22
 */
@SpringBootApplication
public class UserServer {
    private static Logger logger = LoggerFactory.getLogger(UserServer.class);

    public static void main(String[] args) {
        SpringApplication.run(UserServer.class, args);
        logger.info("UserServer start...");
    }
}
