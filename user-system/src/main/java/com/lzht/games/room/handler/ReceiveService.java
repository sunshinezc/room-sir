package com.lzht.games.room.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;

/**
 * Create by weishuaiding on 2018/11/29 19:01
 */
@EnableBinding(Sink.class)
public class ReceiveService {
    private Logger logger = LoggerFactory.getLogger(getClass());

    @StreamListener(Sink.INPUT)
    public void receive(String payload) {
        logger.info("收到消息：{}", payload);
    }
}
